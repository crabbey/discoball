/*
 * Copyright (c) Flashy Lights Ltd 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

'use strict';

module.exports = require('./lib/discoball');
